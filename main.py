import requests
import io
import json
import PyPDF2 as p2

data = {
    "sections": []
}
# streama pdf från länk.
response = requests.get("https://www.africau.edu/images/default/sample.pdf", stream=True)

with io.BytesIO(response.content) as file:
    pdf = p2.PdfFileReader(file)
    
    # antal sidor
    num_pages = pdf.getNumPages()
    # nuvarande sida
    cur_page = 0
    
    # skriv data till json fil, grupperat på sida.
    for page in range(num_pages):
        text = pdf.getPage(page).extract_text()
        pages = text.split('\n\n')
        for page in pages:
            cur_page += 1
            sections = page.split("\n")
            data["sections"].append({
            cur_page: sections
            })
    
    
# skriv till fil.
with open("data.json", "w") as file:
    json.dump(data, file)